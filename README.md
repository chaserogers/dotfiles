# dotfiles

## Installation
#### Clone this repository:

github: `git clone https://github.com/chaserogers/dotfiles.git ~/.dotfiles`

gitlab: `git clone https://gitlab.com/chaserogers/dotfiles.git ~/.dotfiles`

#### Install the dotfiles:

`source ~/.dotfiles/install.sh`
