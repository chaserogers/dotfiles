# Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew update
brew upgrade
brew tap homebrew/core

# Install packages
packages=(
  ack
  autoconf
  automake
  bash
  bsdmake
  cmake
  coreutils
  cowsay
  csshx
  elasticsearch
  elixir
  erlang
  fzf
  fortune
  freetype
  gdbm
  ghi
  git
  git-flow
  gnupg2
  go
  graphviz
  helm
  imagemagick
  jfrog-cli
  jenkins
  jpeg
  jq
  kubectl
  kubectx
  leiningen
  libassuan
  libgcrypt
  libgpg-error
  libksba
  libpng
  libtiff
  libtool
  libusb
  libusb-compat
  libyaml
  little-cms2
  macvim
  mercurial
  mysql
  neofetch
  nvm
  openssl
  openvpn
  ossp-uuid
  pcre
  pinentry
  pkg-config
  proctools
  pth
  python3
  ranger
  readline
  redis
  rg
  ruby
  ruby-build
  spark
  tfenv
  tflint
  tmux
  tokyo-cabinet
  tree
  unixodbc
  v8
  watch
  w3m
  wxmac
  xz
  yarn
  yq
)

brew install "${packages[@]}"
brew services start --all

# install vagrant
brew install --cask vagrant
