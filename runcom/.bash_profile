#!/usr/bin/env bash

SCRIPT_PATH=$(readlink $BASH_SOURCE)
DOTFILES_ROOT=$(dirname "$(dirname "$SCRIPT_PATH")")

for DOTFILE in $(find "$DOTFILES_ROOT"/system/); do
  [ -r "$DOTFILE" ] && [ -f "$DOTFILE" ] && source "$DOTFILE";
done

eval "$(/opt/homebrew/bin/brew shellenv)"

# Load git completions
git_completion_script="${HOME}/.git-completion.bash"
test -s $git_completion_script && source $git_completion_script

# load private profile
source ~/.private_profile

# enable forward search in reverse-i-search with <C-S>
stty -ixon

# sets both ignorespace and ignoredupes
HISTCONTROL=ignoreboth

# turn on iterm2_shell_integration
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh
