#!/usr/bin/env bash

ICLOUD_DIR="$HOME/Library/Mobile Documents/com~apple~CloudDocs"
DATE=$(date +"%Y%m%d")

rm -rf "${ICLOUD_DIR}/workspaces/${HOSTNAME}"*

cp -r ~/workspace/ "${ICLOUD_DIR}/workspaces/${HOSTNAME}-${DATE}/"
